baseargs=root=/dev/ram rootfstype=ramfs init=/init console=tty0 earlyprintk fbcon=map:0 vmalloc=496M debug=7 initcall_debug=0 console=ttyS0,115200
findmmc=part size mmc ${disk} 2 config_size; if test "${config_size}" = "800"; then part start mmc ${disk} 2 config_start; setenv mmcdata 3; mmc read ${scriptaddr} ${config_start} ${config_size}; env import ${scriptaddr} 0x1000; else setenv mmcdata 2; fi
loadargs=setenv bootargs "${baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${args} ${localargs} ${configargs}"
loadenv_mmc=load mmc ${disk}:${mmcdata} ${pvscriptaddr} /boot/uboot.txt;
loadenv=setexpr pvscriptaddr ${scriptaddr} + 0x30000; if test ${pv_fstype} = ubi; then loadenv_ubi; else run loadenv_mmc; fi; setenv pv_try; env import -b ${pvscriptaddr} 0x400; if env exists pv_try; then if env exists pv_trying && test ${pv_trying} = ${pv_try}; then setenv pv_trying; saveenv; setenv boot_rev ${pv_rev}; else setenv pv_trying ${pv_try}; saveenv; setenv boot_rev ${pv_trying}; fi; else setenv boot_rev ${pv_rev}; fi;
loadstep_mmc=run loadenv; load mmc ${disk}:${mmcdata} ${loadaddr} /trails/${boot_rev}/.pv/pv-kernel.img; load mmc ${disk}:${mmcdata} ${rdaddr} /trails/${boot_rev}/.pv/pv-initrd.img; setenv rd_size ${filesize}; setexpr rd_offset ${rdaddr} + ${rd_size}; setenv i 0; while load mmc ${disk}:${mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}; do setexpr i ${i} + 1; setexpr rd_size ${rd_size} + ${filesize}; setexpr rd_offset ${rd_offset} + ${filesize}; done
loadstep_ubi=ubi part ${pv_ubipart}; ubifsmount ubi0:${pv_ubipart}; run loadenv; ubifsload ${loadaddr} /trails/${boot_rev}/.pv/pv-kernel.img; ubifsload ${rdaddr} /trails/${boot_rev}/.pv/pv-initrd.img;
loadstep=if test ${pv_fstype} = ubi; then loadstep_ubi; else run loadstep_mmc; fi
bootcmd=run findmmc; run loadstep; setenv bootargs "${baseargs} pv_try=${pv_try} pv_rev=${boot_rev} panic=2 ${args}"; run loadargs; bootm ${loadaddr} ${rdaddr}:${rd_size}; echo "Failed to boot step, rebooting"; sleep 1; reset
pv_fstype=mmc
mmcboot=1
mmcdata=2
rdaddr=0x86000000
uenvcmd=run bootcmd
boot1=run selectmmc; run loadbootenv; run importenv; run bootcmd
selectmmc=if run checksd; then echo Boot from SD ; setenv disk 1; setenv partition 1:1;else echo Boot from eMMC; setenv disk 0; setenv partition 0:1 ; fi

# disable high mem reallocation by all 1 bits
fdt_high=0xffffffffffffffff
initrd_high=0xffffffffffffffff

